const config = require('./config');
const mysql = require('./sql');
const xl = require('excel4node');

const getUsers = () => {
    return new Promise(
        (resolve, reject) => {
            mysql.connection.query('SELECT id, firstname AS firstName, lastname AS lastName, promotion, repartition_group AS `group` FROM `user` WHERE promotion IN (\'DFASM1\', \'DFASM2\', \'DFASM3\')',
                [],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject();
                    } else {
                        result.forEach(
                            (user) => {
                                switch (user.promotion) {
                                    case 'DFASM1' :
                                        user.promotion = 2;
                                        break;
                                    case 'DFASM2' :
                                        user.promotion = 3;
                                        break;
                                    case 'DFASM3':
                                        user.promotion = 4;
                                        break;
                                }
                            }
                        );
                        resolve(result);
                    }
                });
        }
    );
};

class Table {
    constructor() {
        this.days = [];
    }
}

class Day {
    constructor(date) {
        this.date = date;
        this.weekend = date.getDay() === 0 || date.getDay() === 6; // check if day is Sunday or Saturday
        this.attributions = new Map(); // <terrainId, user>

        config.terrains.forEach(
            (terrain) => {
                this.attributions.set(terrain.id, null);
            }
        );
    }
}

const createTable = () => {
    const table = new Table();

    const beginDate = new Date(config.planning.beginDate);
    const endDate = new Date(config.planning.endDate);

    table.days.push(new Day(beginDate));
    let currentDate = new Date(beginDate.getTime());
    while (true) {
        currentDate.setDate(currentDate.getDate() + 1);

        table.days.push(new Day(new Date(currentDate.getTime())));

        if (currentDate.getDate() === endDate.getDate()
            && currentDate.getMonth() === endDate.getMonth()
            && currentDate.getFullYear() === endDate.getFullYear())
            break;
    }

    return table;
};

class Exception {
    constructor(promotionId) {
        this.dates = [];
        this.promotionId = promotionId;

        this.containsDate = (dateInput) => {
            for (const date of this.dates) {
                if (date.getDate() === dateInput.getDate()
                    && date.getMonth() === dateInput.getMonth()
                    && date.getFullYear() === dateInput.getFullYear())
                    return true;
            }
            return false;
        }
    }
}

const getExceptions = () => {
    const exceptions = new Map();

    for (const exception of config.planning.exceptions) {
        const e = new Exception(exception.promotion);
        for (const date of exception.date) {
            e.dates.push(new Date(date));
        }
        exceptions.set(exception.promotion, e);
    }
    return exceptions;
};

const formatTo2Digits = (number) => {
    return (number < 10 && number > 0) ? '0' + number : number;
};
const formatDate = (date) => {
    return formatTo2Digits(date.getDate()) + '/' + formatTo2Digits(date.getMonth() + 1) + '/' + date.getFullYear().toString().slice(-2);
};
const tableToXL = (table, users) => {
    const wb = new xl.Workbook({
        defaultFont: {
            size: 10,
            name: 'Arial',
            color: '#000000',
            bold: true,
        },
    });

    // TABLEAU
    (() => {
        let ws = wb.addWorksheet('Tableau', {});
        ws.column(1).setWidth(21);
        for (let i = 1; i < 8; i++) {
            ws.column(i + 1).setWidth(23);
        }

        const days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

        const addHeader = (rowId, weekId) => {
            ws.row(rowId).setHeight(21);
            ws.cell(rowId, 3, rowId, 6, true)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    border: {
                        top: {
                            style: 'medium'
                        },
                        right: {
                            style: 'medium'
                        },
                        left: {
                            style: 'medium'
                        },
                        bottom: {
                            style: 'medium'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#ffff99'
                    },
                    font: {
                        size: 12
                    }
                })
                .string('SEMAINE DU ' + formatDate(table.days[weekId * 7].date) + ' au ' + formatDate(table.days[weekId * 7 + 6].date));

            ws.row(rowId + 2).setHeight(18);
            ws.row(rowId + 3).setHeight(18);

            const horaireStyle = wb.createStyle({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                border: {
                    top: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    }
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#666699'
                },
                font: {
                    size: 8
                }
            });
            ws.cell(rowId + 2, 1, rowId + 2, 8, true)
                .style(horaireStyle)
                .string('HORAIRES');
            ws.cell(rowId + 3, 1, rowId + 3, 3, true)
                .style(horaireStyle)
                .string('SEMAINE : 18h30 -00h ou 18h30 - 08h30 (nuit complete)');
            ws.cell(rowId + 3, 4, rowId + 3, 5, true)
                .style(horaireStyle)
                .string('SAMEDI : 12h30 - 00h ou 12h30 - 8h30 (nuit complete)');
            ws.cell(rowId + 3, 6, rowId + 3, 8, true)
                .style(horaireStyle)
                .string('DIMANCHE : 8h30 - 00h ou 8h30 - 8h30 (nuit complete)');

            ws.row(rowId + 4).setHeight(18);
            ws.row(rowId + 5).setHeight(18);

            const topStyle = wb.createStyle({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#ffffff'
                },
                border: {
                    top: {
                        style: 'thin',
                        color: '#000000'
                    },

                    right: {
                        style: 'thin',
                        color: '#000000'
                    },
                    bottom: {
                        style: 'none',
                        color: '#000000'
                    },
                    left: {
                        style: 'thin',
                        color: '#000000'
                    },
                }
            });
            const bottomStyle = wb.createStyle({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#ffffff'
                },
                border: {
                    top: {
                        style: 'none',
                        color: '#000000'
                    },

                    right: {
                        style: 'thin',
                        color: '#000000'
                    },
                    bottom: {
                        style: 'thin',
                        color: '#000000'
                    },
                    left: {
                        style: 'thin',
                        color: '#000000'
                    },
                }
            });

            ws.cell(rowId + 4, 1)
                .style(topStyle)
                .string('DATE');

            ws.cell(rowId + 5, 1)
                .style(bottomStyle)
                .string('SERVICE');


            for (let dayId = 0; dayId < days.length; dayId++) {
                ws.cell(rowId + 4, dayId + 2)
                    .style(topStyle)
                    .string(days[dayId].toUpperCase());

                const day = table.days[(weekId * days.length) + dayId];
                ws.cell(rowId + 5, dayId + 2)
                    .style(bottomStyle)
                    .string(formatDate(day.date));
            }

        };

        const terrainAmountPerPage = 6;
        const headerRowAmount = 6;
        const terrainRowAmount = 3;

        let currentRowId = 1;
        const weekNb = table.days.length / 7;

        for (let weekId = 0; weekId < weekNb; weekId++) {
            console.log("week = " + weekId);

            let i = 0;
            while (i < config.terrains.length || (i % terrainAmountPerPage) !== 0) {
                if ((i % terrainAmountPerPage) === 0) {
                    if (weekId !== 0 || i !== 0)
                        currentRowId++; // ADD ROW BELOW PAGE
                    addHeader(currentRowId, weekId);
                    currentRowId += headerRowAmount;
                }

                ws.row(currentRowId).setHeight(14);
                ws.row(currentRowId + 1).setHeight(24);
                ws.row(currentRowId + 2).setHeight(49);


                const terrain = config.terrains[i];

                if (terrain) {
                    ws.cell(currentRowId, 1, currentRowId + 2, 1, true)
                        .style({
                            alignment: {
                                horizontal: 'center',
                                vertical: 'center',
                                wrapText: true
                            },
                            fill: {
                                type: 'pattern',
                                patternType: 'solid',
                                fgColor: '#efeff0'
                            },
                            border: {
                                left: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                right: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                top: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                bottom: {
                                    style: 'thin',
                                    color: '#000000'
                                }
                            },
                            font: {
                                size: 9
                            }
                        })
                        .string(config.terrains[i].name.toUpperCase() + (config.terrains[i].night ? '\n(Nuit complète)' : ''));
                } else {
                    ws.cell(currentRowId, 1, currentRowId + 2, 1, true)
                        .style({
                            alignment: {
                                horizontal: 'center',
                                vertical: 'center',
                                wrapText: true
                            },
                            fill: {
                                type: 'pattern',
                                patternType: 'solid',
                                fgColor: '#a5a5a5'
                            },
                            border: {
                                left: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                right: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                top: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                bottom: {
                                    style: 'thin',
                                    color: '#000000'
                                }
                            },
                            font: {
                                size: 9
                            }
                        })
                        .string('');
                }

                for (let dayId = (weekId * days.length); dayId < (weekId * days.length + days.length); dayId++) {
                    const day = table.days[dayId];

                    const weekDayId = dayId % 7;

                    if (day && terrain) {
                        const user = day.attributions.get(terrain.id);
                        ws.cell(currentRowId, weekDayId + 2)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                border: {
                                    top: {
                                        style: 'thin'
                                    },
                                    right: {
                                        style: 'thin'
                                    },
                                    left: {
                                        style: 'thin'
                                    },
                                    bottom: {
                                        style: 'none'
                                    }
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: '#ffff99'
                                },
                                font: {
                                    bold: true,
                                    color: '#dd0806'
                                }
                            })
                            .number(user.id);
                        ws.cell(currentRowId + 1, weekDayId + 2)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: (weekDayId === 5 ? '#cbfcfd' : (weekDayId === 6 ? '#90b67d' : '#ffffff'))
                                },
                                border: {
                                    left: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    right: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    top: {
                                        style: 'none',
                                    },
                                    bottom: {
                                        style: 'hair',
                                        color: '#000000'
                                    }
                                },
                            })
                            .string(user.firstName + " " + user.lastName);
                        ws.cell(currentRowId + 2, weekDayId + 2)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: (weekDayId === 5 ? '#cbfcfd' : (weekDayId === 6 ? '#90b67d' : '#ffffff'))
                                },
                                border: {
                                    left: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    right: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    top: {
                                        style: 'none',
                                    },
                                    bottom: {
                                        style: 'thin',
                                        color: '#000000'
                                    }
                                },
                            })
                            .string('');
                    } else {
                        ws.cell(currentRowId, weekDayId + 2, currentRowId + 2, weekDayId + 2, true)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: '#a5a5a5'
                                },
                                border: {
                                    left: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    right: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    top: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    bottom: {
                                        style: 'thin',
                                        color: '#000000'
                                    }
                                },
                                font: {
                                    size: 9
                                }
                            })
                            .string('');
                    }
                }

                currentRowId += terrainRowAmount;
                i++;
            }
        }
    })();

    const getPromotionName = (id) => {
        switch (id) {
            case 2:
                return 'DFASM 1';
            case 3:
                return 'DFASM 2';
            case 4:
                return 'DFASM 3';
        }
    };

    // COMPTAGE
    (() => {
        ws = wb.addWorksheet('COMPTAGE (Réservé DAM)', {});
        ws.column(1).setWidth(7);
        ws.column(2).setWidth(7);
        ws.column(3).setWidth(9);
        ws.column(4).setWidth(6);
        ws.column(5).setWidth(40);
        ws.column(6).setWidth(20);
        ws.column(7).setWidth(15);

        ws.cell(1, 1, 1, 7, true)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                }
            })
            .string('DFASM 1 - DFASM 2 - DFASM 3');
        ws.cell(2, 1, 2, 7, true)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#fcf333'
                },
                font: {
                    color: '#de412c',
                    bold: true,
                },
                border: {
                    left: {
                        style: 'medium',
                    },
                    right: {
                        style: 'medium',
                    },
                    top: {
                        style: 'medium',
                    },
                    bottom: {
                        style: 'medium',
                    }
                },
            })
            .string('ZONE RESERVEE A LA DAM (Ne pas modifier)');
        ws.cell(3, 1)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                }
            })
            .string('SAM');
        ws.cell(3, 2)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                }
            })
            .string('DIM');
        ws.cell(3, 3)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('TOTAL');
        ws.cell(3, 4)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium',
                    }
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#ffff99'
                },
            })
            .string('N°');
        ws.cell(3, 5)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('NOM ET PRENOM');
        ws.cell(3, 6)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('ANNEE');
        ws.cell(3, 7)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('GROUPE');

        const delta = 4;
        for (let userId = 0; userId < users.length; userId++) {
            const user = users[userId];

            ws.cell(userId + delta, 1)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'dotted'
                        },
                        bottom: {
                            style: 'dotted'
                        },
                        right: {
                            style: 'dotted'
                        }
                    }
                })
                .number(user.saturdayAmount);
            ws.cell(userId + delta, 2)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'medium'
                        },
                        bottom: {
                            style: 'medium'
                        },
                        left: {
                            style: 'dotted'
                        },
                        right: {
                            style: 'dotted'
                        }
                    }
                })
                .number(user.sundayAmount);
            ws.cell(userId + delta, 3)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'medium'
                        },
                        bottom: {
                            style: 'medium'
                        },
                        right: {
                            style: 'medium'
                        },
                        left: {
                            style: 'dotted'
                        }
                    }
                })
                .number(user.attribAmount);
            ws.cell(userId + delta, 4)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'none'
                        },
                        bottom: {
                            style: 'dotted'
                        },
                        right: {
                            style: 'thin'
                        },
                        left: {
                            style: 'medium',
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#ffff99'
                    },
                })
                .number(user.id);
            ws.cell(userId + delta, 5)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'thin'
                        },
                        right: {
                            style: 'thin'
                        },
                        bottom: {
                            style: 'thin'
                        },
                        left: {
                            style: 'thin',
                        }
                    }
                })
                .string(user.lastName.toUpperCase() + ' ' + user.firstName);
            ws.cell(userId + delta, 6)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'thin'
                        },
                        right: {
                            style: 'thin'
                        },
                        bottom: {
                            style: 'thin'
                        },
                        left: {
                            style: 'thin',
                        }
                    }
                })
                .string(getPromotionName(user.promotion));
            ws.cell(userId + delta, 7)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'thin'
                        },
                        right: {
                            style: 'thin'
                        },
                        bottom: {
                            style: 'thin'
                        },
                        left: {
                            style: 'thin',
                        }
                    }
                })
                .string(user.group ? user.group : 'Non défini');
        }
    })();

    // LISTE ALPHA
    (() => {
        ws = wb.addWorksheet('LISTE ALPHA (Réservé DAM)', {});
        const defaultCellStyle = wb.createStyle({
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            fill: {
                type: 'pattern',
                patternType: 'solid',
                fgColor: '#ffffff'
            },
            border: {
                left: {
                    style: 'thin',
                    color: '#000000'
                },
                right: {
                    style: 'thin',
                    color: '#000000'
                },
                top: {
                    style: 'thin',
                    color: '#000000'
                },
                bottom: {
                    style: 'thin',
                    color: '#000000'
                }
            },
        });
        const defaultCellStyleBolder = wb.createStyle({
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            fill: {
                type: 'pattern',
                patternType: 'solid',
                fgColor: '#ffffff'
            },
            border: {
                left: {
                    style: 'thin',
                    color: '#000000'
                },
                right: {
                    style: 'thin',
                    color: '#000000'
                },
                top: {
                    style: 'thin',
                    color: '#000000'
                },
                bottom: {
                    style: 'thin',
                    color: '#000000'
                }
            },
            font: {
                bold: true
            }
        });

        ws.column(1).setWidth(5);
        ws.column(2).setWidth(20);
        ws.column(3).setWidth(20);
        ws.column(4).setWidth(20);
        ws.column(5).setWidth(20);

        ws.cell(1, 1, 1, 5, true)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#fcf305'
                },
                font: {
                    bold: true
                }
            })
            .string('DFASM 1 - DFASM 2 - DFASM 3');
        ws.cell(3, 1)
            .style(defaultCellStyleBolder)
            .string('ID');
        ws.cell(3, 2)
            .style(defaultCellStyleBolder)
            .string('Nom');
        ws.cell(3, 3)
            .style(defaultCellStyleBolder)
            .string('Prénom');
        ws.cell(3, 4)
            .style(defaultCellStyleBolder)
            .string('Promotion');
        ws.cell(3, 5)
            .style(defaultCellStyleBolder)
            .string('Groupe');

        for (let userId = 0; userId < users.length; userId++) {
            ws.cell(userId + 4, 1)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#fcf305'
                    },
                    border: {
                        left: {
                            style: 'thin',
                            color: '#000000'
                        },
                        right: {
                            style: 'thin',
                            color: '#000000'
                        },
                        top: {
                            style: 'thin',
                            color: '#000000'
                        },
                        bottom: {
                            style: 'thin',
                            color: '#000000'
                        }
                    },
                    font: {
                        bold: true
                    }
                })
                .number(users[userId].id);
            ws.cell(userId + 4, 2)
                .style(defaultCellStyle)
                .string(users[userId].lastName);
            ws.cell(userId + 4, 3)
                .style(defaultCellStyle)
                .string(users[userId].firstName);
            ws.cell(userId + 4, 4)
                .style(defaultCellStyle)
                .string(getPromotionName(users[userId].promotion));
            ws.cell(userId + 4, 5)
                .style(defaultCellStyleBolder)
                .string(users[userId].group ? users[userId].group : 'Non défini');
        }
    })();

    wb.write('Planning.xlsx');
};

const main =
    async () => {
        const exceptions = getExceptions();
        const users = await getUsers();
        let id = 1;
        const dfasm1 = users
            .filter((user) => user.promotion === 2)
            .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));
        const dfasm2 = users
            .filter((user) => user.promotion === 3)
            .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));
        const dfasm3 = users
            .filter((user) => user.promotion === 4)
            .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));

        const others = dfasm2.concat(dfasm1);
        const all = dfasm3.concat(dfasm2).concat(dfasm1);
        all.forEach((user) => {
            user.id = id++;
            user.attribAmount = 0;
            user.saturdayAmount = 0;
            user.sundayAmount = 0;
        });

        const table = createTable();

        //STEP 1 : REPART DFASM 3 (No weekend, no night, only 5)
        let lastUserIndex = 0;
        for (let day of table.days) {
            if (!day.weekend && !exceptions.get(4).containsDate(day.date)) {
                for (let terrain of config.terrains) {
                    if (!terrain.night) {
                        if (dfasm3[lastUserIndex].attribAmount < 5) {
                            day.attributions.set(terrain.id, dfasm3[lastUserIndex]); // replace
                            dfasm3[lastUserIndex].attribAmount++;
                            lastUserIndex++;
                            lastUserIndex %= dfasm3.length;
                        } else {
                            break;
                        }
                    }
                }

                if (dfasm3[dfasm3.length - 1].attribAmount === 5) {
                    break;
                }
            }
        }

        //STEP 1 : REPART DFASM 1 & DFASM 2 (All terrains, amount not limited, min amount 10)
        lastUserIndex = 0;
        const rejected = [];
        for (let day of table.days) {
            for (let terrain of config.terrains) {
                if (!day.attributions.get(terrain.id)) {

                    let user;

                    const defaultAttrib = () => {
                        user = others[lastUserIndex];

                        while (exceptions.get(user.promotion).containsDate(day.date)) {
                            rejected.push(lastUserIndex);

                            lastUserIndex++;
                            lastUserIndex %= others.length;

                            user = others[lastUserIndex];
                        }
                        day.attributions.set(terrain.id, user);
                        user.attribAmount++;
                        if (day.date.getDay() === 6) {
                            user.saturdayAmount++;
                        } else if (day.date.getDay() === 0) {
                            user.sundayAmount++
                        }


                        lastUserIndex++;
                        lastUserIndex %= others.length;
                    };

                    if (rejected.length !== 0) {
                        let attrib = false;

                        for (const rejectedUserIndex of rejected) {
                            user = others[rejectedUserIndex];
                            if (!exceptions.get(user.promotion).containsDate(day.date)) {
                                day.attributions.set(terrain.id, user);
                                user.attribAmount++;
                                if (day.date.getDay() === 6) {
                                    user.saturdayAmount++;
                                } else if (day.date.getDay() === 0) {
                                    user.sundayAmount++
                                }

                                rejected.splice(rejected.indexOf(rejectedUserIndex), 1);
                                attrib = true;
                                break;
                            }
                        }

                        if (!attrib)
                            defaultAttrib();
                    } else {
                        defaultAttrib();
                    }
                }
            }
        }

        tableToXL(table, all);
    };

main()
    .then(() => {
        mysql.connection.end();
        return console.log('Répartition effectuée avec succès');
    })
    .catch((error) => {
        mysql.connection.end();
        return console.error(error);
    });

