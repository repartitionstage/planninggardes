const config = require('./config');
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: config.sql.host,
    user: config.sql.user,
    password: config.sql.pass,
    database: config.sql.database,
    multipleStatements: true,
    typeCast: true
});

module.exports = {
    mysql,
    connection
};
