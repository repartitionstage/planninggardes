const terrains = [
    {
        id: 0,
        name: 'U.S.I.C.',
        night: false
    },
    {
        id: 1,
        name: 'Urgences gynécologiques',
        night: false
    },
    {
        id: 2,
        name: 'Urgences pédiatriques 1',
        night: false
    },
    {
        id: 3,
        name: 'Urgences pédiatriques 2',
        night: false
    },
    {
        id: 4,
        name: 'SMUR 1',
        night: true
    },
    {
        id: 5,
        name: 'SMUR 2',
        night: true
    },
    {
        id: 6,
        name: 'SAUPA 1',
        night: true
    },
    {
        id: 7,
        name: 'SAUPA 2',
        night: false
    },
    {
        id: 8,
        name: 'SAUPA 3',
        night: false
    },
    {
        id: 9,
        name: 'SAUPA 4',
        night: false
    },
    {
        id: 10,
        name: 'SAUV 1',
        night: true
    },
    {
        id: 11,
        name: 'SAUV 2',
        night: false
    },
    {
        id: 12,
        name: 'PINEL',
        night: false
    },
    {
        id: 13,
        name: 'Réanimation médicale',
        night: false
    },
    {
        id: 14,
        name: 'Réanimation chirurgicale',
        night: false
    },
    {
        id: 15,
        name: 'Réanimation CTV',
        night: false
    },
    {
        id: 16,
        name: 'B.U.',
        night: false
    }
];

module.exports = {
    terrains,
    planning: {
        beginDate: '2019-09-30', //American date format : year-month-day
        endDate: '2020-09-27', //American date format : year-month-day
        exceptions: [
            {
                promotion: 2,
                date: ['2020-1-5', '2020-3-29', '2020-6-28']
            },
            {
                promotion: 3,
                date: ['2020-1-6', '2020-3-30', '2020-6-29']
            },
            {
                promotion: 4,
                date: ['2019-11-04', '2019-11-05', '2019-11-06', '2019-11-07']
            }
        ],
    },
    sql: {
        host: 'qilat.fr',
        port: '3306',
        user: 'qilat',
        pass: '5b3BA6Y8cf',
        database: 'medecin1_stages',
    },
};
